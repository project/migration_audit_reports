<?php

/**
 * @file
 * Export content scrore records in csv.
 */

/**
 * Implements hook_drush_command().
 */
function content_score_drush_command() {
  $items['content-score-report-export'] = array(
    'description' => 'Checks for webform components that should be encrypted.',
    'aliases' => array('csre'),
  );

  return $items;
}

/**
 * Callback for the content-score-report-export command.
 */
function drush_content_score_report_export() {
  drush_print("Content Score report exporting...\n");

  module_load_include('inc', 'content_score', 'includes/content_score.admin');
  $filepath = content_score_reports_export();
  $file_url = str_replace(DRUPAL_ROOT, '', $filepath);

  drush_print("Content Score report exported successfully...\n");
  drush_print("You can download report from <site_url>" . $file_url . "\n");

}
