CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Entities List
 * Maintainers

INTRODUCTION
------------

To get started, enable the migrate_audit_report module and then browse to 'admin/config/migration-report/report-form'
Select the entity from dropdown and click on 'Download Report' button. A csv will get generated for selected Entity.
If you want report for all listed entities then, select 'Export All Entities' option and click on 'Download Report'.

This module contains 'content_score' as a submodule. Content_score will analyze all the Nodes for any inline css, js or img added directly in body field.

USAGES
------
- USING ADMIN LOGIN - 
- Enable the module after download.
- Go to "Configuration >> Migration Audit Report >> Get Report" to download the report directly.
- Go to "Configuration >> Migration Audit Report >> Content Score" to check the inline css, js & images added directly into Body field for node.

- USING DRUSH COMMAND - 
- Use "drush audit-report-export" command to generate the report.

Note - Drush Command generated report gets deleted automatically after 1 day.
	   To Prevent the report from deletion set variable "file_delete_after_day" as "-1".

ENTITIES LIST
-------------
1) Content Data - Available
2) Content type - Available
3) Fields & Field Types - Available
4) Blocks + Custom Blocks - Available
5) Menus - Available
6) Vocabulary and Taxonomy Terms - Available
7) Image Styles - TBD
8) Users - Available
9) Views List + Views Display - Available
10) Roles - Available
11) Permissions Matrix - Available
12) Paragraphs - Available
13) Language - Available
14) Webforms and fields - Available
15) Webform Submissions - Available
16) Files / media uploaded - Available
17) Custom Modules & Features - Available

REQUIREMENTS
------------

- Module has dependency with "phpexcel" module and "phpexcel" library.
- Content_score module has dependency with 'Views'.

MAINTAINERS
-----------

Current maintainers:
 * Ravikant V. Mane (ravimane23) - https://www.drupal.org/u/ravimane23
