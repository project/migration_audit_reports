<?php

/**
 * @file
 * Contains pfizer-uuid-duplicate command implementation.
 */

/**
 * Implements hook_drush_command().
 */
function migration_audit_report_drush_command() {

  $items['migration-audit-report-export'] = array(
    'description' => 'Checks for webform components that should be encrypted.',
    'aliases' => array('audit-report-export'),
  );

  return $items;
}

/**
 * Callback for the migration-audit-report-export command.
 */
function drush_migration_audit_report_export() {
  drush_print("Migrating audit report exporting...\n");

  module_load_include('inc', 'migration_audit_report', 'includes/export_all_data');
  $filepath = migration_audit_report_export_all_data();
  $file_url = str_replace(DRUPAL_ROOT, '', $filepath);

  drush_print("Migrating audit report exported successfully...\n");
  drush_print("You can download report from <site_url>" . $file_url . "\n");

}
