<?php

/**
 * Callback function to export users data in csv.
 */
function migration_audit_webform_data() {

  $users = migration_get_webform();

  // Output into a csv file
  $filename = 'webform_component.csv';
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=' . $filename);
  $csv_file = fopen("php://output", "w");

  fputcsv($csv_file, $users['header']);
  foreach($users['data'] as $key => $record) {
    $data = [];
    if (is_object($record)) {
      $data = (array)$record;
    }

    if (!empty($data)) {
      fputcsv($csv_file, $data, ',', '"');
    }
  }

  fclose($csv_file);
  exit();
}

/**
 * Function to get data from database.
 */
function migration_get_webform() {

  $fields = [
    'node' => [
      'nid' => 'Node Id',
      'title' => 'Title',
    ],
    'webform_component' => [
      'cid' => 'Component ID',
      'form_key' => 'Field machine name',
      'name' => 'Field Name',
      'type' => 'Field Type',
      'value' => 'Default Value',
      'extra' => 'Extra field configs',
      'required' => 'Required',
      'weight' => 'Weight',
    ]
  ];

  $result = [];
  if (module_exists('webform')) {
    // Fetching webform data
    $query = db_select('node', 'n');
    $query->leftjoin('webform_component', 'wc', 'n.nid = wc.nid');
    $query->fields('n', array_keys($fields['node']));
    $query->fields('wc', array_keys($fields['webform_component']))
      ->execute();
    $result = $query->execute()->fetchAll();
  }

  $header = $fields['node'] + $fields['webform_component'];

  return array('header' => $header, 'data' => $result);
}

/**
 * Function to return file data and column headers.
 */
function export_webform_data_entity() {
  $content_type = migration_get_webform();

  $count = count($content_type['header']);
  $content_data = array(
    array_values($content_type['header']),
    array_values($content_type['header'])
  );
  foreach ($content_type['data'] as $data_obj) {
    if (is_object($data_obj)) {
      $data_obj = (array)$data_obj;
    }
    array_push($content_data, array_values($data_obj));
  }

  return array($content_data, $count);
}
