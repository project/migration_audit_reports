<?php

/**
 * Callback function to export users data in csv.
 */
function migration_audit_webform_submission_data() {

  $webform_data = migration_get_webform_submissions_data();

  // Output into a csv file
  $filename = 'webform_submission_data.csv';
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=' . $filename);
  $csv_file = fopen("php://output", "w");

  fputcsv($csv_file, $webform_data['header']);
  foreach($webform_data['data'] as $key => $record) {
    $data = [];
    if (is_object($record)) {
      $data = (array)$record;
    }

    if (!empty($data)) {
      fputcsv($csv_file, $data, ',', '"');
    }
  }

  fclose($csv_file);
  exit();
}

/**
 * Function to get data from database.
 */
function migration_get_webform_submissions_data() {

  $fields = [
    'node' => [
      'title' => 'Title',
    ],
    'webform_component' => [
      'name' => 'Field Name',
      'type' => 'Field Type',
    ],
    'webform_submitted_data' => [
      'data' => 'Data',
    ],
    'webform_submissions' => [
      'submitted' => 'Submitted',
      'completed' => 'Completed',
      'modified' => 'Modified',
      'remote_addr' => 'Submitted IP',
    ]
  ];
  $result = [];
  if (module_exists('webform')) {
    // Webform Submitted Data.
    $query = db_select('webform_component', 'wc');
    $query->leftjoin('webform_submitted_data', 'wsd', 'wsd.cid = wc.cid');
    $query->leftjoin('webform_submissions', 'ws', 'ws.sid = wsd.sid');
    $query->fields('wc', array_keys($fields['webform_component']));
    $query->fields('wsd', array_keys($fields['webform_submitted_data']));
    $query->fields('ws', array_keys($fields['webform_submissions']))
      ->execute();
    $result = $query->execute()->fetchAll();
  }

  $header = $fields['webform_component'] + $fields['webform_submitted_data'] + $fields['webform_submissions'];

  return array('header' => $header, 'data' => $result);
}

/**
 * Function to return file data and column headers.
 */
function export_webform_submission_data_entity() {
  $content_type = migration_get_webform_submissions_data();
  $count = count($content_type['header']);
  $content_data = array(
    array_values($content_type['header']),
    array_values($content_type['header'])
  );
  foreach ($content_type['data'] as $data_obj) {
    if (is_object($data_obj)) {
      $data_obj = (array)$data_obj;
    }
    array_push($content_data, array_values($data_obj));
  }

  return array($content_data, $count);
}
